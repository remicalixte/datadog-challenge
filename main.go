package main

import (
	"errors"
	"flag"
	"fmt"
	"io/ioutil"
	"log"

	"gitlab.com/remicalixte/datadog-challenge/back"
	"gitlab.com/remicalixte/datadog-challenge/ui"
	"gitlab.com/remicalixte/datadog-challenge/watcher"
)

var configFileName = flag.String("config", "config.yml", "specify the name of the configuration file to use")
var mode = flag.String("mode", "default", "mode of execution: 'default' launch the service and display the ui, 'service' only launch the service and 'ui' only display the ui and connect to a remote service")

func main() {

	flag.Parse()

	configFile, err := ioutil.ReadFile(*configFileName)
	if err != nil {
		fmt.Println(err)
		log.Fatalln(errors.New("main: failed to read from config file"))
	}
	config, err := parseConfig(configFile)
	if err != nil {
		fmt.Println(err)
		log.Fatalln(errors.New("main: failed to parse config file"))
	}

	b, err := back.New(
		config.Db.Address,
		config.Db.Username,
		config.Db.Password,
		config.Db.Database,
	)
	if err != nil {
		fmt.Println(err)
		log.Fatalln(errors.New("main: failed to start back"))
	}

	w := watcher.New(config.Websites)
	b.Input = w.Output

	b.Start()
	w.Start()
	defer w.Stop()
	defer b.Stop()
	ui.Run(b.Output)
}
