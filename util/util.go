package util

// Contain checks in a list of strings contains a string in linear time
func Contain(l []string, s string) bool {
	for _, v := range l {
		if v == s {
			return true
		}
	}
	return false
}

// Avg computes the average value of a slice of float64
func Avg(values []float64) float64 {
	var avg float64
	for _, v := range values {
		avg += v
	}
	return avg / float64(len(values))
}

// Max computes the maximum value of a slice of float64
func Max(values []float64) float64 {
	if len(values) == 0 {
		return 0
	}
	var max = values[0]
	for _, v := range values {
		if v > max {
			max = v
		}
	}
	return max
}
