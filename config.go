package main

import (
	yaml "gopkg.in/yaml.v2"
)

type db struct {
	Address  string
	Database string
	Username string
	Password string
}

type config struct {
	Db       db
	Websites []map[string]string
}

func parseConfig(file []byte) (*config, error) {
	c := config{}
	err := yaml.Unmarshal(file, &c)
	if err != nil {
		return nil, err
	}
	return &c, nil
}
