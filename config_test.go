package main

import (
	"fmt"
	"reflect"
	"testing"
)

func TestParseConfig(t *testing.T) {
	expected := config{
		Db: db{
			Address:  "http://influxdb:8086",
			Username: "username",
			Password: "password",
			Database: "monitoring",
		},
		Websites: []map[string]string{
			map[string]string{
				"address":  "http://test.org",
				"interval": "1s",
			},
		},
	}
	fmt.Println("db:\n  address: http://influxdb:8086\n  username: username\n  password: password\n  database: monitoring\nwebsites:\n  - address: http://test.org\n    interval: 1s")
	c, err := parseConfig([]byte(
		"db:\n  address: http://influxdb:8086\n  username: username\n  password: password\n  database: monitoring\nwebsites:\n  - address: http://test.org\n    interval: 1s",
	))
	if err != nil {
		t.Errorf(err.Error())
	} else {
		if !reflect.DeepEqual(expected, *c) {
			t.Errorf("TestParseConfig failed: %s %s", expected, *c)
		}
	}
}
