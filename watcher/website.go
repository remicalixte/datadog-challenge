package watcher

import (
	"fmt"
	"net/http"
	"time"

	"gitlab.com/remicalixte/datadog-challenge/back"
)

var timeout = 3 * time.Second

// WebsiteChecker requests a website, computes metrics and send them to the given output channel
type WebsiteChecker struct {
	Address string
	ticker  *time.Ticker
	done    chan struct{}
	quit    chan struct{}
}

// newWebsiteChecker creates a new WebsiteChecker struct
func newWebsiteChecker(address string, CheckInterval time.Duration) *WebsiteChecker {
	wc := &WebsiteChecker{
		Address: address,
		ticker:  time.NewTicker(CheckInterval),
	}
	wc.done = make(chan struct{})
	wc.quit = make(chan struct{})
	return wc
}

// check makes a request and send metrics to output
func (website *WebsiteChecker) check(output chan back.Metric) {
	start := time.Now()
	client := http.Client{
		Timeout: timeout,
	}
	resp, err := client.Get(website.Address)
	latency := time.Since(start)
	if err != nil {
		output <- back.Metric{
			Time: start,
			Tags: map[string]string{
				"address": website.Address,
			},
			Fields: map[string]interface{}{
				"latency":   int64(latency),
				"available": 0,
			},
		}
	} else {
		available := 0
		if resp.StatusCode/100 == 2 {
			available = 1
		}
		output <- back.Metric{
			Time: start,
			Tags: map[string]string{
				"address": website.Address,
			},
			Fields: map[string]interface{}{
				fmt.Sprintf("code_%d", resp.StatusCode): 1,
				"latency":   int64(latency),
				"available": available,
			},
		}
	}
}

// run starts continuous reporting
func (website *WebsiteChecker) run(output chan back.Metric) {
	defer close(website.done)
	defer website.ticker.Stop()
	for {
		select {
		case <-website.quit:
			return
		case <-website.ticker.C:
			website.check(output)
		}
	}
}

// stop the WebsiteChecker
func (website *WebsiteChecker) stop() {
	close(website.quit)
	<-website.done
}
