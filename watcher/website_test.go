package watcher

import (
	"testing"
	"time"

	"gitlab.com/remicalixte/datadog-challenge/back"
)

var address = "http://google.com"
var interval = 1 * time.Second

func TestNewWebsiteChecker(t *testing.T) {
	w := newWebsiteChecker(address, interval)

	if w.Address != address {
		t.Errorf("TestNewWebsiteChecker failed: %s %s", address, w.Address)
	}

	if w.done == nil || w.quit == nil {
		t.Errorf("TestNewWebsiteChecker failed: nil channel")
	}

}

func TestCheck(t *testing.T) {
	w := newWebsiteChecker(address, interval)
	output := make(chan back.Metric)
	timeout := time.NewTimer(2 * time.Second)
	go w.check(output)

	select {
	case m := <-output:
		if _, ok := m.Tags["address"]; !ok {
			t.Errorf("TestCheck failed: missing address")
		}
		if _, ok := m.Fields["latency"]; !ok {
			t.Errorf("TestCheck failed: missing latency")
		}
		if _, ok := m.Fields["available"]; !ok {
			t.Errorf("TestCheck failed: missing availability")
		}
	case <-timeout.C:
		t.Errorf("TestCheck failed: timeout")
	}

}

func TestRun(t *testing.T) {
	w := newWebsiteChecker(address, interval)
	output := make(chan back.Metric)
	timeout := time.NewTimer(2 * time.Second)
	go w.run(output)
	select {
	case <-output:
		w.stop()
	case <-timeout.C:
		t.Errorf("TestRun failed: timeout")
	}

}
