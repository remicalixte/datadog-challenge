package watcher

import (
	"fmt"
	"time"

	"gitlab.com/remicalixte/datadog-challenge/back"
)

// Watcher makes continuous requests to the websites given in the configuration file and send in to its output
type Watcher struct {
	websites []*WebsiteChecker
	Output   chan back.Metric
}

// New creates a new Watcher, monitoring the given websites
func New(websites []map[string]string) *Watcher {
	w := &Watcher{}
	w.websites = make([]*WebsiteChecker, 0, len(websites))
	for _, website := range websites {
		interval, err := time.ParseDuration(website["interval"])
		if err != nil {
			fmt.Println(err)
			continue
		} else {
			w.websites = append(w.websites, newWebsiteChecker(website["address"], interval))
		}
	}
	w.Output = make(chan back.Metric, 40)
	return w
}

// Start the Watcher jobs
func (watcher Watcher) Start() {
	for _, website := range watcher.websites {
		go website.run(watcher.Output)
	}
}

// Stop the Watcher jobs
func (watcher *Watcher) Stop() {
	for _, website := range watcher.websites {
		website.stop()
	}
	close(watcher.Output)
}
