package watcher

import (
	"testing"
	"time"
)

func TestNewWatcher(t *testing.T) {
	w := New([]map[string]string{
		map[string]string{
			"address":  "http://google.com",
			"interval": "1s",
		},
		map[string]string{
			"address":  "http://other.com",
			"interval": "wrong syntax",
		},
	})
	if len(w.websites) != 1 {
		t.Errorf("TestNewWatcher failed: wrong website parsing")
	}
}

func TestStart(t *testing.T) {
	w := New([]map[string]string{
		map[string]string{
			"address":  "http://google.com",
			"interval": "1s",
		},
		map[string]string{
			"address":  "http://other.com",
			"interval": "wrong syntax",
		},
	})
	done := make(chan struct{})
	timeout := time.NewTimer(5 * time.Second)
	go func() {
		w.Start()
		w.Stop()
		close(done)
	}()

	select {
	case <-done:
	case <-timeout.C:
		t.Errorf("TestStart failed: timeout")
	}

}
