package main

import (
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"time"
)

func root(w http.ResponseWriter, r *http.Request) {
	n := rand.Intn(100)
	if n < 50 {
		http.Error(w, "internal error", 500)
	} else {
		fmt.Fprintln(w, "hello")
	}
}

func main() {
	rand.Seed(time.Now().UnixNano())
	http.HandleFunc("/", root)
	log.Fatalln(http.ListenAndServe(":80", nil))
}
