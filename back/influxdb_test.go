package back

import (
	"reflect"
	"testing"

	"github.com/influxdata/influxdb/models"
)

func TestFormatStat(t *testing.T) {
	res := formatStat([]models.Row{
		models.Row{
			Tags: map[string]string{
				"address": "http://google.com",
			},
			Columns: []string{
				"test",
			},
			Values: [][]interface{}{
				[]interface{}{
					3,
				},
			},
		},
	})
	expected := map[string]map[string][]interface{}{
		"http://google.com": map[string][]interface{}{
			"test": []interface{}{
				3,
			},
		},
	}
	if !reflect.DeepEqual(res, expected) {
		t.Errorf("TestFormatStat failed")
	}
}

func TestFormatAlert(t *testing.T) {
	res := formatAlert([]models.Row{
		models.Row{
			Tags: map[string]string{
				"address": "http://google.com",
			},
			Columns: []string{
				"test",
			},
			Values: [][]interface{}{
				[]interface{}{
					3,
				},
			},
		},
	})
	expected := []map[string]interface{}{
		map[string]interface{}{
			"test": 3,
		},
	}
	if !reflect.DeepEqual(res, expected) {
		t.Errorf("TestFormatStat failed")
	}
}
