package back

import "time"

// Metric stores data of a website's response. The format is built with compatibility with the influxdb client in mind
type Metric struct {
	Time   time.Time              // time of the request
	Tags   map[string]string      // set of tags, usually only contains "address" -> address of the website
	Fields map[string]interface{} // set of metrics from the response
}

// Report stores data to send to the front
type Report struct {
	Label string      // type of the report
	Data  interface{} // data payload
}
