package back

import (
	"fmt"
	"time"
)

// sender fetches data regurlarly and send it to its output channel
type sender struct {
	output          chan Report
	quit            chan struct{}
	done            chan struct{}
	table           string
	retentionPolicy string
	ticker          *time.Ticker
	timeframe       time.Duration
	maxAge          time.Duration
}

// newSender creates a sender struct
func newSender(table, retentionPolicy string, interval, timeframe, maxAge time.Duration) *sender {
	s := &sender{}

	s.table = table
	s.retentionPolicy = retentionPolicy
	s.ticker = time.NewTicker(interval)
	s.timeframe = timeframe
	s.maxAge = maxAge

	s.quit = make(chan struct{})
	s.done = make(chan struct{})

	return s
}

// start the sender, data are fetched with the function given as argument
func (s *sender) start(dataGetter func() (Report, error)) {
	defer close(s.done)
	defer s.ticker.Stop()
	for {
		select {
		case <-s.quit:
			return
		case <-s.ticker.C:
			res, err := dataGetter()
			if err != nil {
				fmt.Println(err)
			} else {
				s.output <- res
			}
		}
	}
}

//stop the sender
func (s *sender) stop() {
	close(s.quit)
	<-s.done
}
