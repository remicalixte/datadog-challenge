package back

import (
	"reflect"
	"testing"
	"time"
)

var table = "test"
var interval = time.Second
var timeframe = time.Second
var maxAge = time.Second

func TestNewSender(t *testing.T) {
	s := newSender(table, "", interval, timeframe, maxAge)

	if table != s.table {
		t.Errorf("TestNewSender failed: %s %s", table, s.table)
	}
	if timeframe != s.timeframe {
		t.Errorf("TestNewSender failed: %s %s", timeframe, s.timeframe)
	}
	if maxAge != s.maxAge {
		t.Errorf("TestNewSender failed: %s %s", maxAge, s.maxAge)
	}
}

func TestSenderStart(t *testing.T) {
	s := newSender(table, "", interval, timeframe, maxAge)
	s.output = make(chan Report)
	timeout := time.NewTimer(2 * interval)
	r := Report{
		Label: "test",
		Data:  "data",
	}
	go s.start(func() (Report, error) {
		return r, nil
	})
	select {
	case res := <-s.output:
		if !reflect.DeepEqual(r, res) {
			t.Errorf("TestSenderStart failed: report not sent")
		}
	case <-timeout.C:
		t.Errorf("TestSenderStart failed: timeout")
	}

}
