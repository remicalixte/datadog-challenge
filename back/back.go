package back

import (
	"errors"
	"fmt"
	"time"
)

const (
	// Parameters of the raw data measurement
	rawTable = "website"

	// Parameters of the short-term metrics
	shortTable           = "stats_short"
	shortRetentionPolicy = "short_term"
	shortInterval        = 10 * time.Second
	shortTimeframe       = 2 * time.Minute
	shortMaxAge          = 10 * time.Minute

	// Parameters of the long-term metrics
	longTable           = "stats_long"
	longRetentionPolicy = "long_term"
	longInterval        = 1 * time.Minute
	longTimeframe       = 10 * time.Minute
	longMaxAge          = 1 * time.Hour

	// Parameters of the alerts
	alertTable = "stats_alert"
)

// Back listens for Metric structs sent in its Input channel and continuously sends Report struct into its Output channel
type Back struct {
	senders []*sender
	Input   chan Metric
	Output  chan Report
	db      *influxdb
}

// New creates a new Back struct
func New(address, username, password, database string) (*Back, error) {
	b := &Back{}

	b.Output = make(chan Report)

	b.senders = []*sender{
		newSender(shortTable, shortRetentionPolicy, shortInterval, shortTimeframe, shortMaxAge),
		newSender(longTable, longRetentionPolicy, longInterval, longTimeframe, longMaxAge),
		newSender(alertTable, "", shortInterval, 0, 0),
	}

	for _, s := range b.senders {
		s.output = b.Output
	}

	db, err := newDB(address, username, password, database)
	if err != nil {
		return nil, err
	}
	b.db = db
	return b, nil
}

// Start the Back jobs
func (b *Back) Start() {
	go b.listen()
	go b.report()
}

// listen for inputs to write in the database
func (b *Back) listen() {
	for metric := range b.Input {
		err := b.db.write(rawTable, metric.Tags, metric.Fields, metric.Time)
		if err != nil {
			fmt.Println(err)
		}
	}
}

// report metrics to the Ouput
func (b *Back) report() {

	// create retention policy to discard old data. retention is at least of one hour
	err := b.db.updateRetentionPolicy(shortRetentionPolicy, 1*time.Hour)
	if err != nil {
		fmt.Println(err)
		fmt.Println(errors.New("report: fail to update retention policy"))
	}
	err = b.db.updateRetentionPolicy(longRetentionPolicy, 2*time.Hour)
	if err != nil {
		fmt.Println(err)
		fmt.Println(errors.New("report: fail to update retention policy"))
	}

	// create continuous query to compute metrics and alerts
	err = b.db.updateContinuousQuery(shortTable, shortRetentionPolicy, generalmetrics, shortTimeframe, shortInterval)
	if err != nil {
		fmt.Println(err)
		fmt.Println(errors.New("report: fail to update continuous query"))
	}
	err = b.db.updateContinuousQuery(longTable, longRetentionPolicy, generalmetrics, longTimeframe, longInterval)
	if err != nil {
		fmt.Println(err)
		fmt.Println(errors.New("report: fail to update continuous query"))
	}

	// start senders that report data to the client
	// the first one sends short-term metrics
	go b.senders[0].start(func() (Report, error) {
		data, err := b.db.getStat(b.senders[0].table, b.senders[0].retentionPolicy, b.senders[0].maxAge)
		if err != nil {
			return Report{}, err
		}
		return Report{
			Label: "shortMetric",
			Data:  data,
		}, nil
	})

	// the second one sends long-term metrics
	go b.senders[1].start(func() (Report, error) {
		data, err := b.db.getStat(b.senders[1].table, b.senders[1].retentionPolicy, b.senders[1].maxAge)
		if err != nil {
			return Report{}, err
		}
		return Report{
			Label: "longMetric",
			Data:  data,
		}, nil
	})

	// the last one sends alerts
	go b.senders[2].start(func() (Report, error) {
		err := b.db.generateAlert()
		if err != nil {
			fmt.Println(err)
		}
		data, err := b.db.getAlert(0)
		if err != nil {
			return Report{}, err
		}
		return Report{
			Label: "alert",
			Data:  data,
		}, nil
	})
}

// Stop the Back jobs
func (b *Back) Stop() {
	for _, s := range b.senders {
		s.stop()
	}
}
