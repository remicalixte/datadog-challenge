package back

import (
	"encoding/json"
	"errors"
	"fmt"
	"strings"
	"time"

	"github.com/influxdata/influxdb/client/v2"
	"github.com/influxdata/influxdb/models"
)

const (
	generalmetrics = "mean(latency) as latency_avg, max(latency) as latency_max, count(/code_/), mean(available) as availability" // columns to select for general metrics
	alertMetrics   = "mean(available)"                                                                                            // columns to select to generate alerts
)

// influxdb handle to connection with the influx database
type influxdb struct {
	client       client.Client
	batchPoints  client.BatchPoints
	maxBatchSize int
	database     string
}

// newDB returns an influxb struct instance with given parameters
func newDB(address, username, password, database string) (*influxdb, error) {
	db := &influxdb{}
	db.database = database
	c, err := client.NewHTTPClient(client.HTTPConfig{
		Addr:     address,
		Username: username,
		Password: password,
	})
	if err != nil {
		return nil, err
	}
	db.client = c

	err = db.newBatch()
	if err != nil {
		return nil, err
	}
	db.maxBatchSize = 10
	return db, nil
}

// newBatch renews the batch of the influxdb struct
func (db *influxdb) newBatch() error {
	bp, err := client.NewBatchPoints(client.BatchPointsConfig{
		Database: db.database,
	})
	if err != nil {
		return err
	}
	db.batchPoints = bp
	return nil
}

// write stores the given datapoint for eventual writing in the database. It stores the point in a batch, when the batch reaches its max size, it is written in the database
func (db *influxdb) write(measurement string, tags map[string]string, fields map[string]interface{}, time time.Time) error {
	pt, err := client.NewPoint(measurement, tags, fields, time)
	if err != nil {
		return err
	}
	if db.batchPoints == nil {
		err = db.newBatch()
		if err != nil {
			return err
		}
	}
	db.batchPoints.AddPoint(pt)
	if len(db.batchPoints.Points()) >= db.maxBatchSize {
		err = db.client.Write(db.batchPoints)
		if err != nil {
			return err
		}
		db.batchPoints = nil
	}
	return nil
}

// query performs a query in the database
func (db *influxdb) query(query string) ([]models.Row, error) {
	resp, err := db.client.Query(client.NewQuery(query, db.database, ""))
	if err != nil {
		return nil, err
	}
	if resp.Error() != nil {
		return nil, resp.Error()
	}
	if len(resp.Results) == 0 {
		return nil, errors.New("query: no data from query results")
	}
	return resp.Results[0].Series, err
}

// formatStat formats the result of a query for metrics sending
func formatStat(rawData []models.Row) map[string]map[string][]interface{} {
	data := make(map[string]map[string][]interface{})
	for _, row := range rawData {
		address := row.Tags["address"]
		data[address] = make(map[string][]interface{})
		for _, col := range row.Columns {
			data[address][col] = make([]interface{}, len(row.Values))
		}
		for i, v := range row.Values {
			for j := range row.Columns {
				data[address][row.Columns[j]][i] = v[j]
			}
		}
	}
	return data
}

// formatAlert formats the result of a query for alerts sending
func formatAlert(rawData []models.Row) []map[string]interface{} {
	if len(rawData) == 0 {
		return make([]map[string]interface{}, 0)
	}
	row := rawData[0]
	data := make([]map[string]interface{}, len(row.Values))
	for i, alert := range row.Values {
		data[i] = make(map[string]interface{})
		for j, col := range alert {
			data[i][row.Columns[j]] = col
		}
	}
	return data
}

// getData generates a query with the given parameters
func (db *influxdb) getData(table, retentionPolicy string, limit int, group string, maxAge time.Duration, reverse bool) ([]models.Row, error) {
	var tableQuery string
	if retentionPolicy != "" {
		tableQuery = fmt.Sprintf("select * from %s.%s", retentionPolicy, table)
	} else {
		tableQuery = fmt.Sprintf("select * from %s", table)
	}
	var limitQuery string
	if limit != 0 {
		limitQuery = fmt.Sprintf("limit %d", limit)
	}
	var groupQuery string
	if group != "" {
		groupQuery = fmt.Sprintf("group by %s", group)
	}
	var maxAgeQuery string
	if maxAge != 0 {
		maxAgeQuery = fmt.Sprintf("where time > now() - %s", maxAge)
	}
	var orderQuery string
	if reverse {
		orderQuery = "order by time desc"
	}
	query := strings.Join(
		[]string{
			tableQuery,
			maxAgeQuery,
			groupQuery,
			orderQuery,
			limitQuery,
		},
		" ",
	)
	data, err := db.query(query)
	if err != nil {
		return nil, err
	}
	return data, nil
}

// getStat fetches metrics from the database
func (db *influxdb) getStat(table, retentionPolicy string, maxAge time.Duration) (map[string]map[string][]interface{}, error) {
	data, err := db.getData(table, retentionPolicy, 0, "address", maxAge, false)
	if err != nil {
		return nil, err
	}
	return formatStat(data), nil
}

// getAlert fetches alerts from the database
func (db *influxdb) getAlert(limit int) ([]map[string]interface{}, error) {
	data, err := db.getData("alert", "", limit, "", 0, true)
	if err != nil {
		return nil, err
	}
	return formatAlert(data), nil
}

// generateAlerts creates alerts in the 'alert' measurement when using 'stats_alert' measurement.
// Alerts are generated when a site crosses the 80%-availability threshold
func (db *influxdb) generateAlert() error {
	query := fmt.Sprintf("select last(availability) from %s.%s group by address", shortRetentionPolicy, shortTable)
	alertMetrics, err := db.query(query)
	if err != nil {
		return err
	}
	for _, website := range alertMetrics {
		alertQuery := fmt.Sprintf("select last(status) from alert where address = '%s'", website.Tags["address"])
		previousAlertData, err := db.query(alertQuery)
		if err != nil {
			fmt.Println(err)
			continue
		}
		if len(website.Values) == 0 || len(website.Values[0]) < 2 {
			fmt.Println(errors.New("generateAlert: missing availability"))
		}
		availabilityNumber, ok := website.Values[0][1].(json.Number)
		if !ok {
			continue
		}
		availability, err := availabilityNumber.Float64()
		if err != nil {
			fmt.Println(err)
			continue
		}
		if availability < 0.8 && (len(previousAlertData) == 0 || previousAlertData[0].Values[0][1] == "recover") {
			err := db.write(
				"alert",
				website.Tags,
				map[string]interface{}{
					"status":       "down",
					"availability": availability,
				},
				time.Now(),
			)
			if err != nil {
				fmt.Println(err)
			}
		}
		if availability > 0.8 && len(previousAlertData) != 0 && previousAlertData[0].Values[0][1] == "down" {
			err := db.write(
				"alert",
				website.Tags,
				map[string]interface{}{
					"status":       "recover",
					"availability": availability,
				},
				time.Now(),
			)
			if err != nil {
				fmt.Println(err)
			}
		}
	}
	return nil
}

//updateContinuousQuery drops the current continuous query of the given name in the database and creates a new one with the new parameters
func (db *influxdb) updateContinuousQuery(name, retentionPolicy string, fields string, timeframe time.Duration, resample time.Duration) error {
	dropQuery := fmt.Sprintf("drop continuous query %s on %s", name, db.database)
	createQuery := ""
	if resample == 0 {
		createQuery = fmt.Sprintf(
			"create continuous query %s on %s begin select %s into %s from %s group by time(%s), address end",
			name,
			db.database,
			fields,
			name,
			rawTable,
			timeframe,
		)
	} else {
		createQuery = fmt.Sprintf(
			"create continuous query %s on %s resample every %s begin select %s into %s.%s from %s group by time(%s), address end",
			name,
			db.database,
			resample,
			fields,
			retentionPolicy,
			name,
			rawTable,
			timeframe,
		)
	}
	_, err := db.query(dropQuery)
	if err != nil {
		return err
	}
	_, err = db.query(createQuery)
	if err != nil {
		return err
	}
	return nil
}

//updateRetentionPolicy drops the current retention policy of the given name in the database and creates a new one with the new parameters
func (db *influxdb) updateRetentionPolicy(retentionPolicy string, maxAge time.Duration) error {
	dropQuery := fmt.Sprintf("drop retention policy %s on %s", retentionPolicy, db.database)
	createQuery := fmt.Sprintf("create retention policy %s on %s duration %s replication 1", retentionPolicy, db.database, maxAge)
	_, err := db.query(dropQuery)
	if err != nil {
		return err
	}
	_, err = db.query(createQuery)
	if err != nil {
		return err
	}
	return nil
}
