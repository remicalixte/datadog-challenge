package ui

import (
	ui "github.com/gizak/termui"
)

// component stores a widget and its size in fraction of the total terminal height
type component struct {
	widget *ui.Block
	size   float64
}

// newComponent creates a component struct
func newComponent(widget *ui.Block, size float64) *component {
	c := &component{}
	c.widget = widget
	c.size = size
	return c
}

// updateSize computes the new height of the widget
func (c *component) updateSize() {
	c.widget.Height = int(c.size * float64(ui.TermHeight()))
}
