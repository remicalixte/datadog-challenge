package ui

import (
	"encoding/json"
	"fmt"
	"strings"
	"time"

	"gitlab.com/remicalixte/datadog-challenge/util"

	ui "github.com/gizak/termui"
)

// panel stores the widgets used to display a website's metrics
type panel struct {
	availChart *ui.BarChart
	maxChart   *ui.BarChart
	avgChart   *ui.BarChart
	summary    *ui.List
	components []*component
	title      *ui.Par
	blank      *ui.Par
}

// newPanel creates a panel struct
func newPanel(title string, timeframe int, timeframeUnit string) *panel {
	p := &panel{}

	p.title = ui.NewPar("")
	p.title.BorderLabel = title
	p.title.Border = false
	p.title.Height = 1

	p.blank = ui.NewPar("")
	p.blank.Border = false
	p.blank.Height = 1

	p.availChart = ui.NewBarChart()
	p.availChart.BorderLabel = "Availability (%)"
	p.availChart.BarWidth = 6

	p.avgChart = ui.NewBarChart()
	p.avgChart.BorderLabel = "Average latency (ms)"
	p.avgChart.BarWidth = 6

	p.maxChart = ui.NewBarChart()
	p.maxChart.BorderLabel = "Maximum latency (ms)"
	p.maxChart.BarWidth = 6

	p.summary = ui.NewList()
	p.summary.BorderLabel = "Over the whole period"

	p.components = []*component{
		newComponent(&p.availChart.Block, 1./6),
		newComponent(&p.avgChart.Block, 1./6),
		newComponent(&p.maxChart.Block, 1./6),
		newComponent(&p.summary.Block, 1./6),
	}

	return p
}

// update display of the data
func (p *panel) update(data map[string][]interface{}) {

	p.summary.Items = make([]string, 0, len(data))

	var timestamp []time.Time
	if timeData, ok := data["time"]; ok {
		timestamp = parseTime(timeData)
	}

	if availability, ok := data["availability"]; ok {
		p.updateChart(
			availability,
			p.availChart,
			timestamp,
			" - Availability: %.1f %%",
			util.Avg,
			func(v float64) float64 {
				return v * 100
			})
	}

	if latency, ok := data["latency_avg"]; ok {
		p.updateChart(
			latency,
			p.avgChart,
			timestamp,
			" - Average Latency: %.1f ms",
			util.Avg,
			func(v float64) float64 {
				return v / float64(time.Millisecond)
			})
	}

	if availability, ok := data["latency_max"]; ok {
		p.updateChart(
			availability,
			p.maxChart,
			timestamp,
			" - Maximumm Latency: %.1f ms",
			util.Avg,
			func(v float64) float64 {
				return v / float64(time.Millisecond)
			})
	}

	p.summary.Items = append(p.summary.Items, " - Status code count:")
	for metric := range data {
		if strings.Contains(metric, "count_code_") {
			code := strings.Split(metric, "count_code_")[1]
			var totalCount int
			for _, count := range data[metric] {
				countNumber, ok := count.(json.Number)
				if !ok {
					continue
				}
				c, err := countNumber.Int64()
				if err == nil {
					totalCount += int(c)
				}
			}
			if totalCount > 0 {
				p.summary.Items = append(p.summary.Items, fmt.Sprintf("   - Code %s: %d", code, totalCount))

			}
		}
	}

}

// parseTime parses a slice of timestamp and return a slice of time.Time object
func parseTime(timeData []interface{}) []time.Time {
	var localTimestamp []time.Time
	localTimestamp = make([]time.Time, len(timeData))
	for i, timeString := range timeData {
		timestamp, err := time.Parse(time.RFC3339Nano, timeString.(string))
		if err == nil {
			localTimestamp[i] = timestamp.Local()
		}
	}
	return localTimestamp
}

// updateChart takes new data and update a BarChart widget with it, it also adds an line in the summary widget with an aggragated value (average or max for example)
func (p *panel) updateChart(data []interface{}, chart *ui.BarChart, timestamp []time.Time, template string, aggregate func([]float64) float64, format func(float64) float64) {
	values := make([]float64, len(data))
	chart.Data = make([]int, len(values))
	chart.DataLabels = make([]string, len(values))

	for i, v := range data {
		number, ok := v.(json.Number)
		if !ok {
			continue
		}
		value, err := number.Float64()
		if err != nil {
			continue
		}
		formattedValue := format(value)
		values[i] = formattedValue
		chart.Data[i] = int(formattedValue)
		chart.DataLabels[i] = fmt.Sprintf("%d:%d", timestamp[i].Hour(), timestamp[i].Minute())
	}

	p.summary.Items = append(p.summary.Items, fmt.Sprintf(template, aggregate(values)))

}
