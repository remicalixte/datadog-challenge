package ui

import (
	"encoding/json"
	"fmt"

	"gitlab.com/remicalixte/datadog-challenge/util"

	ui "github.com/gizak/termui"
)

// dashboard stores the state of ui as well as the top level widgets
type dashboard struct {
	list          *ui.List
	alert         *ui.List
	panels        []*panel
	components    []*component
	websiteData   []map[string]map[string][]interface{}
	websites      []string
	selected      int
	selectedColor string
}

// newDashboard creates a new dashboard struct
func newDashboard() *dashboard {
	dashboard := &dashboard{}

	dashboard.selectedColor = "(fg-black,bg-green)"
	dashboard.websiteData = []map[string]map[string][]interface{}{
		make(map[string]map[string][]interface{}),
		make(map[string]map[string][]interface{}),
	}

	dashboard.list = ui.NewList()
	dashboard.list.BorderLabel = "Websites"

	dashboard.alert = ui.NewList()
	dashboard.alert.BorderLabel = "Alerts"

	dashboard.panels = []*panel{
		newPanel("Latest 10 minutes (2 minutes timeframes updated every 10 seconds)", 2, "min"),
		newPanel("Latest hour (10 minutes timeframes updated every minute)", 10, "min"),
	}

	dashboard.components = []*component{
		newComponent(&dashboard.list.Block, 2./3),
		newComponent(&dashboard.alert.Block, 1./3),
	}

	dashboard.updateSize()

	return dashboard
}

//updateSize computes the new heights of all the widgets according to the terminal's height
func (dashboard *dashboard) updateSize() {
	for _, c := range dashboard.components {
		c.updateSize()
	}
	for _, p := range dashboard.panels {
		for _, c := range p.components {
			c.updateSize()
		}
	}
}

// updateWebsite uses the new data to update the display of the websites
func (dashboard *dashboard) updateWebsite(index int, newWebsiteData map[string]map[string][]interface{}) {
	if dashboard.websites == nil {
		dashboard.websites = make([]string, 0, len(newWebsiteData))
		dashboard.list.Items = make([]string, 0, 2*len(newWebsiteData))
	}
	for address, metrics := range newWebsiteData {
		if !util.Contain(dashboard.websites, address) {
			dashboard.websites = append(dashboard.websites, address)
			dashboard.list.Items = append(dashboard.list.Items, fmt.Sprintf(" - %s", address))
			dashboard.list.Items = append(dashboard.list.Items, "")
		}
		if _, ok := dashboard.websiteData[index][address]; ok {
			for metric, values := range metrics {
				dashboard.websiteData[index][address][metric] = values
			}
		} else {
			dashboard.websiteData[index][address] = metrics
		}
	}
	if len(dashboard.websites) != 0 {
		dashboard.list.Items[2*dashboard.selected] = fmt.Sprintf(" - [%s]%s", dashboard.websites[dashboard.selected], dashboard.selectedColor)
		dashboard.panels[index].update(dashboard.websiteData[index][dashboard.websites[dashboard.selected]])

	}
}

// updateAlert uses the new data to update the display of the alerts
func (dashboard *dashboard) updateAlert(alerts []map[string]interface{}) {
	dashboard.alert.Items = make([]string, len(alerts))
	for i, alert := range alerts {
		alertTime, _ := alert["time"].(string)
		availability, _ := alert["availability"].(json.Number).Float64()
		availability = 100 * availability
		if alert["status"] == "down" {
			dashboard.alert.Items[i] = fmt.Sprintf("%s: Website %s is down. [availability of %.1f%%](fg-red)", alertTime, alert["address"], availability)
		}
		if alert["status"] == "recover" {
			dashboard.alert.Items[i] = fmt.Sprintf("%s: Website %s has recovered. [availability of %.1f%%](fg-blue)", alertTime, alert["address"], availability)
		}
	}
}

// move changes to currently focused website
func (dashboard *dashboard) move(step int) {
	if len(dashboard.websites) != 0 {
		dashboard.list.Items[2*dashboard.selected] = fmt.Sprintf(" - %s", dashboard.websites[dashboard.selected])
		dashboard.selected = (dashboard.selected + step + len(dashboard.websites)) % len(dashboard.websites)
		dashboard.list.Items[2*dashboard.selected] = fmt.Sprintf(" - [%s]%s", dashboard.websites[dashboard.selected], dashboard.selectedColor)
		for i, data := range dashboard.websiteData {
			dashboard.panels[i].update(data[dashboard.websites[dashboard.selected]])
		}
	}
}
