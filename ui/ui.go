package ui

import (
	"gitlab.com/remicalixte/datadog-challenge/back"

	ui "github.com/gizak/termui"
)

// Run the console user interface
func Run(input chan back.Report) error {

	if err := ui.Init(); err != nil {
		return err
	}
	defer ui.Close()

	dashboard := newDashboard()

	// get all the widget to display
	ls := dashboard.list

	shortAvailability := dashboard.panels[0].availChart
	shortAvg := dashboard.panels[0].avgChart
	shortMax := dashboard.panels[0].maxChart
	shortTitle := dashboard.panels[0].title
	shortBlank := dashboard.panels[0].blank
	shortSummary := dashboard.panels[0].summary

	longAvailability := dashboard.panels[1].availChart
	longAvg := dashboard.panels[1].avgChart
	longMax := dashboard.panels[1].maxChart
	longTitle := dashboard.panels[1].title
	longBlank := dashboard.panels[1].blank
	longSummary := dashboard.panels[1].summary

	alert := dashboard.alert

	// build layout
	ui.Body.AddRows(
		ui.NewRow(
			ui.NewCol(12, 0, alert)),
		ui.NewRow(
			ui.NewCol(4, 0, ls),
			ui.NewCol(4, 0, shortTitle, shortAvg, shortMax, longTitle, longAvg, longMax),
			ui.NewCol(4, 0, shortBlank, shortAvailability, shortSummary, longBlank, longAvailability, longSummary),
		),
	)

	// calculate layout
	ui.Body.Align()
	ui.Render(ui.Body)

	// resize handler
	ui.Handle("/sys/wnd/resize", func(e ui.Event) {
		ui.Body.Width = ui.TermWidth()
		dashboard.updateSize()
		ui.Body.Align()
		ui.Clear()
		ui.Render(ui.Body)
	})

	// exit handlers
	ui.Handle("/sys/kbd/q", func(ui.Event) {
		ui.StopLoop()
	})

	ui.Handle("/sys/kbd/<escape>", func(ui.Event) {
		ui.StopLoop()
	})

	ui.Handle("/sys/kbd/C-c", func(ui.Event) {
		ui.StopLoop()
	})

	// focus handlers
	ui.Handle("/sys/kbd/<up>", func(ui.Event) {
		dashboard.move(-1)
		ui.Render(ui.Body)
	})

	ui.Handle("/sys/kbd/<down>", func(ui.Event) {
		dashboard.move(1)
		ui.Render(ui.Body)
	})

	// listen to input channel and update display with new data
	go func() {
		for report := range input {
			switch report.Label {
			case "shortMetric":
				dashboard.updateWebsite(0, report.Data.(map[string]map[string][]interface{}))
			case "longMetric":
				dashboard.updateWebsite(1, report.Data.(map[string]map[string][]interface{}))
			case "alert":
				dashboard.updateAlert(report.Data.([]map[string]interface{}))
			}
			ui.Render(ui.Body)
		}
	}()

	// main loop
	ui.Loop()

	return nil
}
