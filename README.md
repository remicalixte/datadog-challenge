# Datadog coding challenge

Console program to monitor website availability and performance

![](example.png)

## Configuration

The websites to monitor can be stored in a configuration file like `config.yml` given as example. By default the file used is named `config.yml` and placed in the same directory than the executable but a custom path name can be specified with the `-config` flag.

## Dependencies

You need `go`, `dep`, `docker` and `docker-compose` installed on your system.

## How to start

To start the database and the test website inside containers run `docker-compose up -d`.
To fetch the dependencies run `dep ensure`, then build the app with `go build` and run it with `./data-challenge`

You can change the selected website in the ui with the up/down arrows.

## Project structure

### Watcher

This package monitors the websites in the configuration by making requests at the given interval. The metrics are send to the back through a channel

### Back

The back receives the data sent by the watcher and handles the connection to the database. It regurlary fetches new data from the database to be displayed by the ui. The chosen database technology is influxdb largely because it enables continuous queries which are helpful to automatically compute new metrics on a regular basis. The retention policies are also useful to delete old unused data.

### UI

This is a console user interface written with the termui library. It displays alerts for all the websites and a menu to select the metrics to display 


The different packages starts go routines to work asynchronously, they receive and send data using channels.

## Further improvements

- The websites are specified in a static configuration file, it could be useful to store them in a database to easily provide a way to add website from the ui
- A web interface would be well suited for this application and could enable more complex display of the data
- The different packages are rather independent but the app is monolithic as it compiles in a single binary that handles frontend and backend operations. It would be better to expose a command line flag to start only the back or the ui and handle communication with websockets for example. It wouldn't be too hard to implement because go channels have a somewhat similar behavior.